import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Leilao {
    private UUID id;
    private List<Lance> lances;

    public Leilao() {
        id = UUID.randomUUID();
        lances = new ArrayList<>();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public Lance adicionarNovoLance(Lance lance){
        Lance lanceValido = validarLance(lance);
        lances.add(lanceValido);
        return lance;
    }

    public Lance validarLance(Lance lance){
        if (lances.size() == 0) {
            return lance;
        }
        if(lance.getValorDoLance()>lances.get(lances.size()-1).getValorDoLance()){
            return lance;
        }else{
            throw new RuntimeException ("O Valor do lance tem que ser maior que " + lances.get(lances.size()-1).getValorDoLance());
        }
    }
}
