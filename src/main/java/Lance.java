import java.util.UUID;

public class Lance {
    private UUID id;
    private Usuario usuario;
    private double valorDoLance;

    public Lance() {
    }

    public Lance(String nomeUsuario, double valorDoLance) {
        id = UUID.randomUUID();
        usuario = new Usuario(nomeUsuario);
        this.valorDoLance = valorDoLance;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public double getValorDoLance() {
        return valorDoLance;
    }

    public void setValorDoLance(double valorDoLance) {
        this.valorDoLance = valorDoLance;
    }
}
