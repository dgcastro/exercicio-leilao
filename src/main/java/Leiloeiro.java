import java.util.UUID;

public class Leiloeiro {
    private UUID id;
    private String nome;
    private Leilao leilao;

    public Leiloeiro(String nome) {
        id = UUID.randomUUID();
        this.nome = nome;
        leilao = new Leilao();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance(){
        if(leilao.getLances().isEmpty()){
            throw new RuntimeException("Nenhum lance foi feito ainda.");
        }
        Lance maiorLance = leilao.getLances().get(leilao.getLances().size()-1);
        return maiorLance;
    }
}
