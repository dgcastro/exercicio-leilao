import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeiloeiroTeste {
    private Leiloeiro leiloeiro;

    @BeforeEach
    public void setUp() {
        leiloeiro = new Leiloeiro("Doug Dimmadome");
    }

    @Test
    public void testeParaRetornarMaiorLance(){
        Lance lanceMenor = new Lance("Johnny", 10.49);
        Lance lanceMaior = new Lance("Bob", 10.50);
        leiloeiro.getLeilao().getLances().add(lanceMenor);
        leiloeiro.getLeilao().getLances().add(lanceMaior);
        Assertions.assertNotNull(leiloeiro.retornarMaiorLance());
    }

    @Test
    public void testeParaCapturarErroMaiorLance(){
        Assertions.assertThrows(RuntimeException.class, () -> {leiloeiro.retornarMaiorLance();});
    }
}
