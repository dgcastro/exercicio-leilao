import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeilaoTeste {
    Leilao leilao;

    @BeforeEach
    public void setUP(){
        leilao = new Leilao();
    }

    @Test
    public void testeParaAdicionarLanceValido(){
        Lance lance = new Lance("Johnny", 10.49);
        Assertions.assertNotNull(leilao.adicionarNovoLance(lance));
    }

    @Test
    public void testeParaCapturarErroLance(){
        Lance lanceMenor = new Lance("Johnny", 10.49);
        Lance lanceMaior = new Lance("Bob", 10.50);
        leilao.adicionarNovoLance(lanceMaior);
        Assertions.assertThrows(RuntimeException.class, () -> {leilao.validarLance(lanceMenor);});
    }

    @Test
    public void testeParaValidarLance(){
        Lance lanceMenor = new Lance("Johnny", 10.49);
        Lance lanceMaior = new Lance("Bob", 10.50);
        leilao.adicionarNovoLance(lanceMenor);
        Assertions.assertNotNull(leilao.validarLance(lanceMaior));
    }

}
